/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inyecciondedependencias;

/**
 *
 * @author Javier Paz
 */
public class Motocicleta implements IVehiculo {
 
	@Override
	public void rodar() {
		System.out.println("Soy una Motocicleta y estoy rodando...");
    
}
}
