/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inyecciondedependencias;

/**
 *
 * @author Javier Paz
 */
public class Principal {
    
    public static void main(String[] args) {
		RodarVehiculo rodarVehiculo = new RodarVehiculo();
		
		IVehiculo bicicleta = new Bicicleta();		
		rodarVehiculo.setVehiculo(bicicleta); //Inyeccion de dependencia
		rodarVehiculo.rodar();
		
		IVehiculo motocicleta = new Motocicleta();		
		rodarVehiculo.setVehiculo(motocicleta); //Inyeccion de dependencia
		rodarVehiculo.rodar();
		
		IVehiculo auto = new Auto();		
		rodarVehiculo.setVehiculo(auto); //Inyeccion de dependencia
		rodarVehiculo.rodar();
    }
}
