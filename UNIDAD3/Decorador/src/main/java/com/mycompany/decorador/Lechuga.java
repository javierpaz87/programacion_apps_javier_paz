/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.decorador;

/**
 *
 * @author Javier Paz
 */
public class Lechuga extends DecoradorHamburguesa{
    private Hamburguesa hamburguesa;
    
    public Lechuga(Hamburguesa h){
        this.hamburguesa=h;
    }
    @Override
    public String getDescripcion(){
        return hamburguesa.getDescripcion()+"+ lechuga";
    }
}
