/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.singleton;

/**
 *
 * @author Javier Paz
 */
public class Main {
    
    public static void main(String[] args) {
        
        Logger logger_1 = Logger.getInstance();
        Logger logger_2 = Logger.getInstance();
        
        logger_1.setValue("Mensaje");
        System.out.println(logger_2.getValue());
    }
    
}
