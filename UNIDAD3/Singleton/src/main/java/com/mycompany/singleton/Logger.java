/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.singleton;

/**
 *
 * @author Javier Paz
 */
public class Logger {
    
    private static Logger log;
    private String value;
    
    private Logger(){
        
    }
    public static Logger getInstance(){
        
        if(log == null){
            log=new Logger();
        }
        
        return log;
    }
    
    public void log (String msg){
        System.out.println(msg);
    }
    public void setValue(String msg){
        value=msg;
    }
    public String getValue(){
        return value;
    }
}
